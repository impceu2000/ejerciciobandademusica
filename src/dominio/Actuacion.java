package dominio;
import java.time.LocalDate;
import java.util.ArrayList;

public class Actuacion {
    public LocalDate fecha;
    public ArrayList<Participante> listaParticipantes = new ArrayList<>();

    public Actuacion(LocalDate fecha) {
        this.fecha = fecha;
    }

    public void annadirParticipante(Participante participante) {
        listaParticipantes.add(participante);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Participante participante : listaParticipantes) {
            sb.append(participante + "\n");
        }
        return "Fecha de actuación: " + fecha + "\n" + sb.toString();
    }


}
