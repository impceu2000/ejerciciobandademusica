package dominio;
import java.math.BigDecimal;

public class MusicoRefuerzo extends Musico {
    private BigDecimal dineroAPagar;

    public MusicoRefuerzo(String nombre, String instrumento, BigDecimal dineroAPagar) {
        super(nombre, instrumento);
        this.dineroAPagar = dineroAPagar;
    }

    public String toString() {
        return super.toString() + "recibe: " + dineroAPagar + " €";
    }
}
