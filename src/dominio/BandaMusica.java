package dominio;

import java.util.ArrayList;

public class BandaMusica {
    public String nombre;

    public BandaMusica(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Actuacion> listaActuaciones = new ArrayList<>();

    public void annadirActuacion(Actuacion actuacion) {
        listaActuaciones.add(actuacion);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Actuacion actuacion : listaActuaciones) {
            sb.append("\n" + actuacion + "\n");
        }
        return nombre + "\n" + sb.toString();
    }



}
